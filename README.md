# Jetson Wheels

## Overview

This repository contains python `.whl` files for Jetson devices. These files are built from source and are not official releases. They are provided as a convenience for users who want to install a specific version of a package on a Jetson device (or a container running on a Jetson device).

## Packages

The following packages are available:

| Package | Version | Python Version | File | SHA256 | Jetson Device | /etc/nv_tegra_release |
| ------- | ------- | -------------- | ---- | ------ | ------------- | --------------------- |
| torch | 1.10.0a0+git71f889c | 3.8 | torch-1.10.0a0+git71f889c-cp38-cp38-linux_aarch64.whl|  06d326e37987b28266c50f3156ab9e8a3223f2410cd44dd83294a48f6aae6ad5| NVIDIA Jetson Nano Developer Kit |# R32 (release), REVISION: 7.4, GCID: 33514132, BOARD: t210ref, EABI: aarch64|
| torch | 1.12.0a0+git664058f | 3.8 | torch-1.12.0a0+git664058f-cp38-cp38-linux_aarch64.whl | 218f518ce987fef169cab924afd8e74f4a44478803ff67f8e733c54a59280d4f | NVIDIA Jetson Nano Developer Kit |# R32 (release), REVISION: 7.4, GCID: 33514132, BOARD: t210ref, EABI: aarch64|
